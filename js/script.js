// listen for a scroll event
$(window).scroll(function(){
    // how far it scrolled from the top
    var wScroll = $(this).scrollTop();
    
    $('.logo').css({
        'transform': 'translate(0px, ' + (wScroll/2)  + '%)'
    });
    
    $('.back-bird').css({
        'transform': 'translate(0px, ' + (wScroll/4) + '%)'
    });
    
    $('.fore-bird').css({
        'transform': 'translate(0px, -' + (wScroll/16)  + '%)'
    });
    
    
    if(wScroll > $('.clothes-pics').offset().top - ($(window).height() / 1.3) ){
        $('.clothes-pics figure').each(function(i){
            
            setTimeout(function(){
                $('.clothes-pics figure').eq(i).addClass('is-showing');
            }, 150 * (i+1));
            
        });
    }
    
    
});